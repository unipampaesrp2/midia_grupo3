package testes;

import java.io.IOException;

import models.Jogo;
import bancos.BancoDeJogos;

/**
 * Classe que testa o banco de jogos.
 * @author victor costa
 *
 */
public class TesteBancoDeJogos {
	public static void main(String[] args) {
		BancoDeJogos jogos = new BancoDeJogos();

		try {
			jogos.le();
		} catch (IOException ioe) {
			System.err
			.println("Arquivo nao encontrado ou corrompido, n�o foi possivel ler o arquivo.\n"
					+ ioe.getMessage());
		}
		
		System.out.println(jogos.exibir());
		
		try {
			jogos.escreve();
		} catch (IOException ioe) {
			System.err
					.println("Arquivo nao encontrado ou corrompido, dados n�o foram salvos.\n"
							+ ioe.getMessage());
		}
	}
}
