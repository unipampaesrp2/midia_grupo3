package testes;

import models.Musica;
import bancos.BancoDeMusicas;

/**
 * Classe que testa o banco de musicas.
 * @author Diovane Freitas
 *
 */
public class TesteBancoDeMusicas {
	public static void main(String[]args){
		BancoDeMusicas musicas = new BancoDeMusicas();
		//musicas.le();
		Musica musica = new Musica("C:/midia/musicas/Wiggle.mp3", "Wiggle", "Musica mais ouvida em 2014", "Hip Hop, Rap", 
				"Jason Desrouleaux & C. Broadus","Jason Derulo ft. Snoop Dogg", 164, "Ingles", 2014);
		Musica musica1 = new Musica("C:/midia/musicas/Talk dirty to me.mp3", "Talk dirty to me", "Umas das melhores desse artista", "Rap", 
				"Jason Derulo","Jason Derulo", 182, "Ingles", 2011);
		Musica musica2 = new Musica("C:/midia/musicas/Hoje.mp3", "Hoje", "Sucesso brasileiro", "Funk", 
				"Ludmilla","Ludmilla", 159, "Portugues", 2013);
		Musica musica3 = new Musica ("C:/midia/musicas/Ei Novinha.mp3", "Ei Novinha", "Sucesso do MMMV", "Funk", 
				"MC Dudu ft Brancoala","MC Dudu", 189, "Portugues", 2012);
		Musica musica4 = new Musica("C:/midia/musicas/Happy.mp3", "Happy", "Destaque de 2014", "Pop", 
				"Pharrell Williams","Pharrell Williams", 143, "Ingles", 2014);
		Musica musica5 = new Musica("C:/midia/musicas/Bad.mp3", "Bad", "DJ David Guetta com a cantora Vassy", "Eletro", 
				"DJ David Guetta", "DJ David Guetta", 151, "Ingles", 2013);
		Musica musica6 = new Musica ("C:/midia/musicas/Pika do ver�o.mp3", "Pika do ver�o", "Sucesso do MMMV", "Funk", 
				"MC Dudu ft. Kondzilla","MC Dudu", 169, "Portugues", 2012);
		Musica musica7 = new Musica("C:/midia/musicas/Canto Alegretense.mp3", "Canto Alegretense", "Homenagem ao munic�pio de Alegrete", "Gaucha", 
				"Nico Fagundes e Bagre Fagundes","Nico Fagundes e Bagre Fagundes", 158, "Portugues", 1983);
		Musica musica8 = new Musica("C:/midia/musicas/Dark Horse.mp3", "Dark Horse", "Can��o gravada para o quarto �lbum da cantora", "Pop", 
				"Katy Perry, Jordan Houston","Katy Perry ft. Juicy J.", 225, "Portugues", 2013);
		Musica musica9 = new Musica ("C:/midia/musicas/Riders on the storm.mp3", "Riders on the storm", "Trilha sonora do jogo NFSU2 da Electronic Arts.", "Rock, Hip Hop", 
				"Jim Morrison, Robby Krieger, Snoop Dogg","The Doors ft. Snoop Dogg", 300, "Ingles", 1971);
		musicas.cadastrar(musica);
		musicas.cadastrar(musica1);
		musicas.cadastrar(musica2);
		musicas.cadastrar(musica3);
		musicas.cadastrar(musica4);
		musicas.cadastrar(musica5);
		musicas.cadastrar(musica6);
		musicas.cadastrar(musica7);
		musicas.cadastrar(musica8);
		musicas.cadastrar(musica9);
		//musicas.ordenarAno();
		//musicas.ordenarTitulo();
		musicas.ordenarNome();
		musicas.escreve();
	}
}
