/**
 * 
 */
package testes;

import java.io.IOException;
import java.util.Scanner;

import bancos.*;

/**
 * Classe para demonstra�ao das classes produzidas no trabalho 3
 * 
 * @author grupo 3
 *
 */
public class Demonstracao {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		BancoDeEbooks ebooks = new BancoDeEbooks();
		BancoDeFilmes filmes = new BancoDeFilmes();
		BancoDeJogos jogos = new BancoDeJogos();
		BancoDeMusicas musicas = new BancoDeMusicas();
		try {
			ebooks.le();
			//filmes.le();
			jogos.le();
			musicas.le();

			System.out.println("Os dados foram recuperados dos arquivos.\n");
		} catch (IOException ioe) {
			System.err.println("Erro na leitura dos arquivos.");
		}
		System.out.println("\nTecle ENTER para continuar...");
		in.nextLine();

		ebooks.cadastrar("C:/midia/ebooks/TheFaultInOurStars.pdf",
				"A culpa e das estrelas", "Henrique e diego", "Pra chorar",
				"Romance", 2013, 321, "rosemary ed", "brasil");

//		filmes.cadastrar("C:/midia/filmes/CentopeiaHumana", "Centopeia Humana",
//				"Gore pra caralho", "Terror", "PT", "Allan",
//				"Adriano, Diovane, Juliano", 160, 2013);

		jogos.cadastrar("C:/midia/jogos/Pitfall.exe", "Pitfall",
				"Jogo classico", "2D", "Autores desconhecidos", 1, false, 1899);

		musicas.cadastrar("C:/midia/musicas/Invisto em dobro.mp3",
				"Invisto em dobro", "Carnaval", "Axe", "Banda Luxuria",
				"Banda luxuria", 260, "portugues", 2014);

		try {
			ebooks.escreve();
			//filmes.escreve();
			jogos.escreve();
			musicas.escreve();
			System.out
					.println("Midias Cadastradas\nTecle ENTER para continuar...");
			in.nextLine();
		} catch (IOException ioe) {
			System.err.println("Erro na grava�ao de arquivos");
		}

		System.out.println(ebooks.exibir());
		//System.out.println(filmes.exibir());
		System.out.println(jogos.exibir());
		System.out.println(musicas.exibir());

		System.out.println("Midias Exibidas\nTecle ENTER para continuar...");
		in.nextLine();

		System.out.println(ebooks.consultar("A culpa e das estrelas"));
		//System.out.println(filmes.consultar("Centopeia Humana"));
		System.out.println(jogos.consultar("Pitfall"));
		System.out.println(musicas.consultar("Invisto em dobro"));

		System.out.println("Midias Consultadas\nTecle ENTER para continuar...");
		in.nextLine();

		ebooks.excluir("A culpa e das estrelas");
		filmes.excluir("Centopeia humana");
		//jogos.excluir("Pitfall");
		musicas.excluir("Invisto em dobro");

		System.out.println("Midias Removidas\nTecle ENTER para continuar...");
		in.nextLine();

		ebooks.ordenarAno();
		filmes.ordenarAno();
		jogos.ordenarAno();
		musicas.ordenarAno();

		try {
			ebooks.escreve();
			//filmes.escreve();
			jogos.escreve();
			musicas.escreve();

			System.out
					.println("Arquivos ordenados por ano\nTecle ENTER para continuar...");
			in.nextLine();
		} catch (IOException ioe) {
			System.err.println("Erro na grava��o dos arquivos");
		}

		ebooks.ordenarTitulo();
		filmes.ordenarTitulo();
		jogos.ordenarTitulo();
		musicas.ordenarTitulo();

		try {
			ebooks.escreve();
//			filmes.escreve();
			jogos.escreve();
			musicas.escreve();

			System.out
					.println("Arquivos ordenados por Titulo\nTecle ENTER para continuar...");
			in.nextLine();
		} catch (IOException ioe) {
			System.err.println("Erro na grava��o dos arquivos");
		}

	}
}
