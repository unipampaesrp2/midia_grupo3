package bancos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

import models.Musica;

/**
 * Classe que manipula e organiza as musicas.
 * 
 * @author Diovane Freitas
 *
 */

public class BancoDeMusicas implements BancoDeMidias {

	private LinkedList<Musica> musicas = new LinkedList<Musica>();
	
	/**
	 * Metodo que cadastra uma musica no banco.
	 */

	public void cadastrar(Musica musica) {
		musicas.add(musica);
	}
	
	/**
	 * Metodo que cadastra uma musica no banco recebendo todas as informa�oes da
	 * musica.
	 * 
	 * @param nomeDoArquivo
	 *            nome do arquivo
	 * @param titulo
	 *            o titulo
	 * @param descricao
	 *            a descri�ao
	 * @param genero
	 *            o genero
	 * @param autores
	 *            os autores
	 * @param interpretes
	 *            os interpretes
	 * @param duracao
	 *            a dura��o
	 * @param idioma
	 * 			  o idioma                                                                                                                                                                                                                                                                                                                                                                                                       
	 * @param ano
	 *            o ano
	 */

	public void cadastrar(String nomeDoArquivo, String titulo,
			String descricao, String genero, String autores,
			String interpretes, int duracao, String idioma, int ano) {

		Musica musica = new Musica(nomeDoArquivo, titulo, descricao, genero,
				autores, interpretes, duracao, idioma, ano);

		musicas.add(musica);
	}
	
	/**
	 * Metodo que exclui uma musica do banco.
	 * 
	 * @param musica
	 *            a musica a ser excluida.
	 */
	public void excluir(Musica musica) {
		musicas.remove(musica);
	}
	
	/**
	 * Metodo que recebe o titulo da musica, busca o no banco e retorna seu
	 * indice, retorna -1 se n�o encontrar.
	 * 
	 * @param titulo
	 *            o titulo da musica a ser procurada.
	 * @return o indice da musica, ou -1 se n�o for encontrada.
	 */
	public int buscaMusica(String titulo) {
		for (int i = 0; i < musicas.size(); i++) {
			if (titulo.equalsIgnoreCase(musicas.get(i).getTitulo())) {
				return i;
			}
		}
		return -1;
	}
	/**
	 * Metodo que remove uma musica do banco pelo seu titulo.
	 * 
	 * @param titulo
	 *            o titulo da musica a ser removida.
	 */
	public void excluir(String titulo) {
		int i = buscaMusica(titulo);
		if (i != -1) {
			musicas.remove(i);
		}
	}

	/**
	 * Metodo que ordena o banco de musicas pelo ano, usando o metodo Selection Sort.
	 */
	public void ordenarAno() {
		for (int fixo = 0; fixo < musicas.size() - 1; fixo++) {
			int menor = fixo;

			for (int i = menor + 1; i < musicas.size(); i++) {
				if (musicas.get(i).getAno() < musicas.get(menor).getAno()) {
					menor = i;
				}
			}
			if (menor != fixo) {
				Musica t = musicas.get(fixo);
				musicas.set(fixo, musicas.get(menor));
				musicas.set(menor, t);
			}
		}
	}
	/**
	 * Metodo que ordena o banco de musicas pelo titulo, usando o metodo Selection Sort.
	 */
	public void ordenarTitulo() {
		for (int fixo = 0; fixo < musicas.size() - 1; fixo++) {
			int menor = fixo;

			for (int i = menor + 1; i < musicas.size(); i++) {
				if (musicas.get(i).getTitulo().compareToIgnoreCase(musicas.get(menor).getTitulo())<0) {
					menor = i;
				}
			}
			if (menor != fixo) {
				Musica t = musicas.get(fixo);
				musicas.set(fixo, musicas.get(menor));
				musicas.set(menor, t);
			}
		}
	}
	/**
	 * Metodo que ordena o banco de musicas por genero, usando o metodo Selection Sort.
	 */
	public void ordenarGenero() {
		for (int fixo = 0; fixo < musicas.size() - 1; fixo++) {
			int menor = fixo;

			for (int i = menor + 1; i < musicas.size(); i++) {
				if (musicas.get(i).getGenero().compareToIgnoreCase(musicas.get(menor).getGenero())<0) {
					menor = i;
				}
			}
			if (menor != fixo) {
				Musica t = musicas.get(fixo);
				musicas.set(fixo, musicas.get(menor));
				musicas.set(menor, t);
			}
		}
	}
	/**
	 * Metodo que ordena o banco de musicas pelo nome, usando o metodo Selection Sort.
	 */
	public void ordenarNome() {
		for (int fixo = 0; fixo < musicas.size() - 1; fixo++) {
			int menor = fixo;

			for (int i = menor + 1; i < musicas.size(); i++) {
				if (musicas.get(i).getNomeDoArquivo().compareToIgnoreCase(musicas.get(menor).getNomeDoArquivo())<0) {
					menor = i;
				}
			}
			if (menor != fixo) {
				Musica t = musicas.get(fixo);
				musicas.set(fixo, musicas.get(menor));
				musicas.set(menor, t);
			}
		}

	}	
	
	
	/**
	 * Metodo que retorna todas as informa�oes da musica pelo titulo.
	 */
	public String consultar(String titulo) {
		int i = buscaMusica(titulo);
		if (i != -1) {
			return musicas.get(i).toString();
		} else {
			return null;
		}
	}
	
	/**
	 * Metodo que edita uma musica recebendo seu titulo atual e o objeto Musica novo.
	 * 
	 * @param titulo
	 *            o titulo anterior.
	 * @param musica
	 *            o novo objeto a substituir o anterior.
	 */
	public void editar(String titulo, Musica musica) {
		int i = buscaMusica(titulo);
		if (i != -1) {
			musicas.set(i, musica);
		}

	}
	/**
	 * Exibe todos os titulos das musicas cadastrados.
	 */
	public String exibir() {
		String titulos = "";
		for (int i = 0; i < musicas.size(); i++) {
			titulos += (musicas.get(i).getTitulo().trim()+"\n");
		}
		return titulos;
	}
	
	/**
	 * Le o arquivo texto e recupera as informa�oes salvas.
	 */
	public void le() {
		try {

			BufferedReader le = new BufferedReader(
					new FileReader("Musicas.txt"));

			int j = Integer.parseInt(le.readLine().trim());
			le.readLine();

			for (int i = 0; i < j; i++) {
				String nomeDoArquivo = le.readLine();
				String titulo = le.readLine();
				String descricao = le.readLine();
				String genero = le.readLine();
				String autores = le.readLine();
				String interpretes = le.readLine();
				int duracao = Integer.parseInt(le.readLine().trim());
				String idioma = le.readLine().trim();
				int ano = Integer.parseInt(le.readLine().trim());
				le.readLine();

				Musica musica = new Musica(nomeDoArquivo, titulo, descricao,
						genero, autores, interpretes, duracao, idioma, ano);
				musicas.add(musica);
			}
			le.close();

		} catch (Exception ioe) {
			System.err.println("Arquivo nao encontrado ou corrompido, leitura n�o efetuada.\n"
							+ ioe.getMessage());
		}

	}

	/**
	 * Grava no arquivo texto e salva as informa�oes.
	 */
	public void escreve() {
		try {
			FileWriter arq = new FileWriter(new File("Musicas.txt"));
			PrintWriter gravarArq = new PrintWriter(arq);
			
			gravarArq.println(musicas.size());
			gravarArq.println("");
			for (int i = 0; i < musicas.size(); i++) {
				gravarArq.printf(musicas.get(i).getNomeDoArquivo() + "%n"
						+ musicas.get(i).getTitulo() + "%n"
						+ musicas.get(i).getDescricao() + "%n"
						+ musicas.get(i).getGenero() + "%n"
						+ musicas.get(i).getAutores() + "%n"
						+ musicas.get(i).getInterpretes() + "%n"
						+ musicas.get(i).getDuracao() + "%n"
						+ musicas.get(i).getIdioma() + "%n"
						+ musicas.get(i).getAno() + "%n%n");
			}

			arq.flush();
			arq.close();
			gravarArq.close();

		} catch (IOException ioe) {
			System.err.println("Arquivo nao encontrado\n" + ioe.getMessage());
		}

	}

	/**
	 * Remove uma musica pelo titulo.
	 */
	public void remover(String titulo) {
		for (int i = 0; i < musicas.size(); i++) {
			if (musicas.get(i).getTitulo().equals(titulo)) {
				musicas.remove(i);
			}
		}

	}

}
