package bancos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import models.EBook;
import models.Filme;

/**
 * Classe que manipula os objetos Ebooks
 * 
 * @author Lincoln
 *
 */
public class BancoDeEbooks implements BancoDeMidias, Serializable {

	public static ArrayList<EBook> ebooks = new ArrayList<>();

	/**
	 * Metodo que cadastra um ebook
	 * 
	 * @param 
	 *            o ebook a ser acadastrado
	 */
	public void cadastrar(EBook ebook) {
		ebooks.add(ebook);
	}

	/**
	 * Metodo que cadastra um ebook recebendo todos os atributos por parametro
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo
	 * @param titulo
	 *            o titulo do ebook
	 * @param autores
	 *            os autores do ebook
	 * @param descricao
	 *            a descricao do ebook
	 * @param genero
	 *            o genero do ebook
	 * @param ano
	 *            o ano do ebook
	 * @param numeroDePaginas
	 *            o numero de paginas do ebook
	 * @param editora
	 *            a editora do ebook
	 * @param local
	 *            o local de edi�ao do ebook
	 */
	public void cadastrar(String nomeDoArquivo, String titulo, String autores,
			String descricao, String genero, int ano, int numeroDePaginas,
			String editora, String local) {

		EBook ebook = new EBook(nomeDoArquivo, titulo, descricao, genero,
				autores, numeroDePaginas, editora, ano, local);

		ebooks.add(ebook);

	}

	/**
	 * Metodo que exclui um ebook do cadastro
	 * 
	 * @param ebook
	 *            o ebook a ser excluido
	 */
	public void excluir(EBook ebook) {

		ebooks.remove(ebook);
	}

	public int validaInt(String mensagem) {
		Scanner entrada = new Scanner(System.in);
		while (true) {
			try {

				System.out.println(mensagem);

				int i = entrada.nextInt();

				return i;

			} catch (InputMismatchException e) {
				System.out.println("Voc� n�o digitou um n�mero v�lido!!!");

				entrada.nextLine();

			} catch (IllegalArgumentException ee) {
				System.out
						.println("Voc� digitou um n�mero negativo, favor digitar novamente!!!");
				entrada.nextLine();
			}
		}

	}

	public String listarTitulos() {
		String titulos = "";
		for (int i = 0; i < ebooks.size(); i++) {
			titulos += (ebooks.get(i).getTitulo() + "\n");
		}
		return titulos;

	}

	public boolean existe(String titulo) {
		for (int i = 0; i < ebooks.size(); i++) {
			if (ebooks.get(i).getTitulo().equalsIgnoreCase(titulo)) {

				return true;
			}

		}
		return false;

	}

	public boolean editar(String titulo, EBook ebook) {

		for (int i = 0; i < ebooks.size(); i++) {
			if (ebooks.get(i).getTitulo().equals(titulo)) {

				ebooks.set(i, ebook);
				return true;

			}

		}
		return false;
	}

	public String validaString(String mensagem) {
		Scanner entrada = new Scanner(System.in);
		String str = new String("");

		while (true) {

			System.out.print(mensagem);
			str = entrada.nextLine();

			if (str.isEmpty()) {

				System.out.println("Este campo n�o pode ficar em branco");
				entrada.nextLine();

			} else {
				return str;
			}
		}

	}

	public String listarEBooks() {
		if (ebooks.size() >= 0) {
			String str = "";

			for (int j = 0; j < ebooks.size(); j++) {
				str += " \n";
				str += ebooks.get(j).toString();
				str += "\n";
				
			}
			return str;

		}

		return null;
	}

	public boolean excluirTitulo(String titulo) {
		for (int j = 0; j < ebooks.size(); j++) {
			if (ebooks.get(j).getTitulo().equalsIgnoreCase(titulo)) {

				ebooks.remove(j);
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo que exclui um ebook do cadastro por titulo
	 * 
	 * @param titulo
	 *            o titulo do ebook
	 */
	public void excluir(String titulo) {
		for (int i = 0; i < ebooks.size(); i++) {
			if (ebooks.get(i).getTitulo().equals(titulo)) {
				ebooks.remove(i);
			}

		}
	}

	public String buscaAutores(String autores) {
		String str = "";
		for (int i = 0; i < ebooks.size(); i++) {

			if (ebooks.get(i).getAutores().equals(autores)) {
				str += "Nome do Arquivo: " + ebooks.get(i).getNomeDoArquivo()
						+ "\n";
				str += "Titulo: " + ebooks.get(i).getTitulo() + "\n";
				str += "Descri��o: " + ebooks.get(i).getDescricao() + "\n";
				str += "G�nero: " + ebooks.get(i).getGenero() + "\n";
				str += "Ano: " + ebooks.get(i).getAno() + "\n";
				str += "Autores: " + ebooks.get(i).getAutores() + "\n";
				str += "Local: " + ebooks.get(i).getLocal() + "\n";
				str += "Editora: " + ebooks.get(i).getEditora() + "\n";
				str += "N�mero de paginas: "
						+ ebooks.get(i).getNumeroDePaginas() + "\n";

			}

		}
		return str;
	}

	public String buscaLocal(String local) {
		String str = "";
		for (int i = 0; i < ebooks.size(); i++) {

			if (ebooks.get(i).getLocal().equals(local)) {
				str += "Nome do Arquivo: " + ebooks.get(i).getNomeDoArquivo()
						+ "\n";
				str += "Titulo: " + ebooks.get(i).getTitulo() + "\n";
				str += "Descri��o: " + ebooks.get(i).getDescricao() + "\n";
				str += "G�nero: " + ebooks.get(i).getGenero() + "\n";
				str += "Ano: " + ebooks.get(i).getAno() + "\n";
				str += "Autores: " + ebooks.get(i).getAutores() + "\n";
				str += "Local: " + ebooks.get(i).getLocal() + "\n";
				str += "Editora: " + ebooks.get(i).getEditora() + "\n";
				str += "N�mero de paginas: "
						+ ebooks.get(i).getNumeroDePaginas() + "\n";

			}

		}
		return str;
	}

	/**
	 * Metodo que ordena os ebooks por titulo
	 */
	public void ordenarTitulo() {
		int i, j;
		EBook eleito;
		for (i = 1; i < ebooks.size(); i++) {
			eleito = ebooks.get(i);
			j = i - 1;
			while ((j >= 0)
					&& (eleito.getTitulo().compareToIgnoreCase(
							ebooks.get(j).getTitulo()) < 0)) {
				ebooks.set(j + 1, ebooks.get(j));
				j--;
			}
			ebooks.set(j + 1, eleito);
		}
	}

	/**
	 * Metodo que ordena os ebooks pelo Nome do arquivo
	 */
	public void ordenarNome() {
		int i, j;
		EBook eleito;
		for (i = 1; i < ebooks.size(); i++) {
			eleito = ebooks.get(i);
			j = i - 1;
			while ((j >= 0)
					&& (eleito.getNomeDoArquivo().compareToIgnoreCase(
							ebooks.get(j).getNomeDoArquivo()) < 0)) {
				ebooks.set(j + 1, ebooks.get(j));
				j--;
			}
			ebooks.set(j + 1, eleito);
		}

	}

	/**
	 * Metodo que ordena os ebooks pelo genero
	 */
	public void ordenarGenero() {
		int i, j;
		EBook eleito;
		for (i = 1; i < ebooks.size(); i++) {
			eleito = ebooks.get(i);
			j = i - 1;
			while ((j >= 0)
					&& (eleito.getGenero().compareToIgnoreCase(
							ebooks.get(j).getGenero()) < 0)) {
				ebooks.set(j + 1, ebooks.get(j));
				j--;
			}
			ebooks.set(j + 1, eleito);
		}

	}

	/**
	 * Metodo que ordena os ebooks pelo ano
	 */
	public void ordenarAno() {
		int i, j;
		EBook eleito;
		for (i = 1; i < ebooks.size(); i++) {
			eleito = ebooks.get(i);
			j = i - 1;
			while ((j >= 0) && (eleito.getAno() < ebooks.get(j).getAno())) {
				ebooks.set(j + 1, ebooks.get(j));
				j--;
			}
			ebooks.set(j + 1, eleito);
		}

	}

	/**
	 * Metodo que busca um ebook pelo titulo
	 * 
	 * @param titulo
	 *            o titulo do ebook que deseja encontrar
	 * @return retorna o indice do ebook ou -1 se n�o encontrar
	 */
	public int buscaEbook(String titulo) {
		for (int i = 0; i < ebooks.size(); i++) {
			if (titulo.equalsIgnoreCase(ebooks.get(i).getTitulo())) {
				return i;
			}
		}
		return -1;
	}

	public String buscaAno(int ano) {
		String str = "";
		for (int i = 0; i < ebooks.size(); i++) {
			if (ebooks.get(i).getAno() == ano) {

				str += "Nome do Arquivo: " + ebooks.get(i).getNomeDoArquivo()
						+ "\n";
				str += "T�tulo: " + ebooks.get(i).getTitulo() + "\n";
				str += "Genero: " + ebooks.get(i).getGenero() + "\n";
				str += "Autores: " + ebooks.get(i).getAutores() + "\n";
				str += "N�mero de P�ginas: "
						+ ebooks.get(i).getNumeroDePaginas() + "\n";
				str += "Editora: " + ebooks.get(i).getEditora() + "\n";
				str += "Ano: " + ebooks.get(i).getAno() + "\n";
			}
		}
		return str;
	}

	public int[] buscaPorEditora(String editora) {
		int aux[] = null;
		int encontrados = 0;
		for (int i = 0; i < ebooks.size(); i++) {
			if (editora.equalsIgnoreCase(ebooks.get(i).getEditora())) {
				encontrados++;
			}
		}
		aux = new int[encontrados];
		for (int i = 0, cE = 0; i < ebooks.size(); i++) {
			if (editora.equalsIgnoreCase(ebooks.get(i).getEditora())) {
				aux[cE] = i;
				cE++;
			}
		}
		return aux;
	}

	/**
	 * Metodo que retorna todas as informa�oes do ebook pelo titulo
	 * 
	 * @param titulo
	 *            o titulo do ebook
	 * @return todas as informa�oes do ebook
	 */
	public String consultar(String titulo) {
		int i = buscaEbook(titulo);
		if (i != -1) {
			return ebooks.get(i).toString();
		} else {
			return null;
		}
	}

	public void editar(String nomeDoArquivo) {
		// TODO Auto-generated method stub

	}

	/**
	 * Metodo que exibe o titulo de todos ebooks cadastrados
	 */
	public String exibir() {
		String titulos = "";
		for (int i = 0; i < ebooks.size(); i++) {
			titulos += ebooks.get(i).getTitulo() + "\n";
		}
		return titulos;
	}

	/**
	 * Metodo que remove um ebook pelo titulo
	 */
	public void remover(String titulo) {
		for (int i = 0; i < ebooks.size(); i++) {
			if (ebooks.get(i).getTitulo().equals(titulo)) {
				ebooks.remove(i);
			}
		}

	}

	/**
	 * Metodo que recupera as informa�oes do arquivo bin
	 * 
	 * @throws ClassNotFoundException
	 */
	public void le(String nome) throws IOException, ClassNotFoundException {
		ObjectInputStream input = new ObjectInputStream(new FileInputStream(
				new File(nome)));
		ebooks = (ArrayList<EBook>) input.readObject();
		input.close();

	}

	/**
	 * Metodo que grava as informa�oes no arquivo bin
	 */
	public void escreve(String nome) throws IOException {
		ObjectOutputStream output = new ObjectOutputStream(
				new FileOutputStream(new File(nome)));
		output.writeObject(ebooks);
		output.close();

	}

	@Override
	public void le() throws IOException {
		//

	}

	@Override
	public void escreve() throws IOException {
		// TODO Auto-generated method stub

	}

}