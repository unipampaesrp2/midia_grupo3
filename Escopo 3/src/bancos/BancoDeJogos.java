package bancos;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;

import models.Jogo;

/**
 * Classe que manipula e organiza os Jogos.
 * 
 * @author victor costa
 *
 */
public class BancoDeJogos implements BancoDeMidias, Serializable {

	private TreeSet<Jogo> jogos = new TreeSet<Jogo>();

	/**
	 * Metodo que cadastra um jogo no banco.
	 * 
	 * @param jogo
	 *            o jogo a ser cadastrado.
	 */
	public void cadastrar(Jogo jogo) {
		jogos.add(jogo);
	}

	/**
	 * Metodo que cadastra um jogo no banco recebendo todas as informa�oes do
	 * jogo.
	 * 
	 * @param nomeDoArquivo
	 *            nome do arquivo.
	 * @param titulo
	 *            o titulo
	 * @param descricao
	 *            a descri�ao
	 * @param genero
	 *            o genero
	 * @param autores
	 *            os autores
	 * @param numeroDeJogadores
	 *            a quantia de jogadores
	 * @param suporteARede
	 *            se existe suporte a rede
	 * @param ano
	 *            o ano
	 */
	public void cadastrar(String nomeDoArquivo, String titulo,
			String descricao, String genero, String autores,
			int numeroDeJogadores, boolean suporteARede, int ano) {

		Jogo jogo = new Jogo(nomeDoArquivo, titulo, descricao, genero, autores,
				numeroDeJogadores, suporteARede, ano);

		jogos.add(jogo);
	}

	/**
	 * Metodo que exclui um jogo do banco.
	 * 
	 * @param jogo
	 *            o jogo a ser excluido.
	 */
	public void excluir(Jogo jogo) {
		jogos.remove(jogo);
	}

	/**
	 * Metodo que remove um jogo do banco pelo seu titulo.
	 * 
	 * @param titulo
	 *            o titulo do jogo a ser removido.
	 */
	public void remover(String titulo) {
		Jogo i = buscaJogo(titulo);
		if (i != null) {
			jogos.remove(i);
		}
	}

	/*
	 * public void ordenarNome() { int h = 1; int n = jogos.size(); while (h <
	 * n) h = h * 3 + 1; h = h / 3; int j; String cc; Jogo c; while (h > 0) {
	 * for (int i = h; i < n; i++) { cc = jogos.get(i).getNomeDoArquivo(); c =
	 * jogos.get(i); j = i; while (j >= h && jogos.get(j -
	 * h).getNomeDoArquivo().compareTo(cc) > 0) { jogos.set(j, jogos.get(j -
	 * h)); j = j - h; } jogos.set(j, c); } h = h / 2; } }
	 * 
	 * 
	 * public void ordenarTitulo() { int h = 1; int n = jogos.size(); while (h <
	 * n) h = h * 3 + 1; h = h / 3; int j; String cc; Jogo c; while (h > 0) {
	 * for (int i = h; i < n; i++) { cc = jogos.get(i).getTitulo(); c =
	 * jogos.get(i); j = i; while (j >= h && jogos.get(j -
	 * h).getTitulo().compareTo(cc) > 0) { jogos.set(j, jogos.get(j - h)); j = j
	 * - h; } jogos.set(j, c); } h = h / 2; } }
	 * 
	 * 
	 * public void ordenarGenero() { int h = 1; int n = jogos.size(); while (h <
	 * n) h = h * 3 + 1; h = h / 3; int j; String cc; Jogo c; while (h > 0) {
	 * for (int i = h; i < n; i++) { cc = jogos.get(i).getGenero(); c =
	 * jogos.get(i); j = i; while (j >= h && jogos.get(j -
	 * h).getGenero().compareTo(cc) > 0) { jogos.set(j, jogos.get(j - h)); j = j
	 * - h; } jogos.set(j, c); } h = h / 2; } }
	 * 
	 * public void ordenarAno() { int h = 1; int n = jogos.size(); while (h < n)
	 * h = h * 3 + 1; h = h / 3; int cc, j; Jogo c; while (h > 0) { for (int i =
	 * h; i < n; i++) { cc = jogos.get(i).getAno(); c = jogos.get(i); j = i;
	 * while (j >= h && jogos.get(j - h).getAno() > cc) { jogos.set(j,
	 * jogos.get(j - h)); j = j - h; } jogos.set(j, c); } h = h / 2; } }
	 */

	/**
	 * Metodo que recebe o titulo do jogo, busca o no banco e retorna seu
	 * indice, retorna -1 se n�o encontrar.
	 * 
	 * @param titulo
	 *            o titulo do jogo a ser procurado.
	 * @return o indice do jogo, ou -1 se n�o encontrado.
	 */
	public Jogo buscaJogo(String titulo) {

		Iterator<Jogo> iterator = jogos.iterator();

		while (iterator.hasNext()) {
			if (titulo.equalsIgnoreCase(iterator.next().getTitulo())) {
				return iterator.next();
			}
		}
		return null;
	}

	/**
	 * Metodo que retorna todas as informa�oes do jogo pelo titulo.
	 */
	public String consultar(String titulo) {
		Jogo buscado = buscaJogo(titulo);
		if (buscado != null) {
			return buscado.toString();
		} else {
			return null;
		}
	}

	/**
	 * Exibe todos os titulos dos jogos cadastrados.
	 */
	public String exibir() {
		Iterator<Jogo> iterator = jogos.iterator();
		String titulos = "";
		while (iterator.hasNext()) {
			titulos += (iterator.next().getTitulo() + "\n");
		}
		return titulos;
	}
	
	public String exibirDoAutor(String autor){
		Iterator<Jogo> iterator = jogos.iterator();
		String titulos="";
		while (iterator.hasNext()) {Jogo aux = iterator.next();
			if (autor.equalsIgnoreCase(aux.getAutores())) {
				titulos += aux.getTitulo()+"\n";
			}
		}
		return titulos;
	}
	
	public String exibirDoGenero(String genero){
		Iterator<Jogo> iterator = jogos.iterator();
		String titulos="";
		while (iterator.hasNext()) {
			Jogo aux = iterator.next();
			if (genero.equalsIgnoreCase(aux.getGenero())) {
				titulos += aux.getTitulo()+"\n";
			}
		}
		return titulos;
	}
	
	public String exibirDoAno(int ano){
		Iterator<Jogo> iterator = jogos.iterator();
		String titulos="";
		while (iterator.hasNext()) {
			Jogo aux = iterator.next();
			if (ano == aux.getAno()) {
				titulos += aux.getTitulo()+"\n";
			}
		}
		return titulos;
	}

	/**
	 * Le o arquivo texto e recupera as informa�oes salvas.
	 */
	public void le() throws IOException {

		BufferedReader le = new BufferedReader(new FileReader("jogos.txt"));

		int j = Integer.parseInt(le.readLine().trim());
		le.readLine();
		for (int i = 0; i < j; i++) {
			String nomeDoArquivo = le.readLine();
			String titulo = le.readLine();
			String descricao = le.readLine();
			String genero = le.readLine();
			String autores = le.readLine();
			int numeroDeJogadores = Integer.parseInt(le.readLine().trim());
			String suporteARedeString = le.readLine().trim();
			int ano = Integer.parseInt(le.readLine().trim());
			le.readLine();
			boolean suporteARede;
			if (suporteARedeString.equalsIgnoreCase("true")) {
				suporteARede = true;
			} else {
				suporteARede = false;
			}

			Jogo jogo = new Jogo(nomeDoArquivo, titulo, descricao, genero,
					autores, numeroDeJogadores, suporteARede, ano);
			jogos.add(jogo);
		}
		le.close();
	}

	/**
	 * Grava no arquivo texto e salva as informa�oes.
	 */
	public void escreve() throws IOException {
		FileWriter arq = new FileWriter(new File("jogos.txt"));
		PrintWriter gravarArq = new PrintWriter(arq);

		gravarArq.println(jogos.size());
		gravarArq.println("");
		
		Iterator<Jogo> iterator = jogos.iterator();
		while(iterator.hasNext()){
			Jogo j = iterator.next();
			gravarArq.printf(j.getNomeDoArquivo() + "%n"
					+ j.getTitulo() + "%n"
					+ j.getDescricao() + "%n"
					+ j.getGenero() + "%n"
					+ j.getAutores() + "%n"
					+ j.getNumeroDeJogadores() + "%n"
					+ j.isSuporteARede() + "%n"
					+ j.getAno() + "%n%n");
		}
		
		arq.flush();
		arq.close();
		gravarArq.close();
	}


	public void ordenarNome() {
		// TODO Auto-generated method stub
		
	}

	public void ordenarTitulo() {
		// TODO Auto-generated method stub
		
	}

	public void ordenarGenero() {
		// TODO Auto-generated method stub
		
	}

	public void ordenarAno() {
		// TODO Auto-generated method stub
		
	}

}
