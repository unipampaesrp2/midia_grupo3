package bancos;

import java.io.IOException;

/**
 * Interface que tem todos os metodos obrigatorios a um banco de midias.
 * 
 * @author grupo 3
 *
 */
public interface BancoDeMidias {

	public void le() throws IOException;

	public void escreve() throws IOException;

	public void remover(String titulo);

	public void ordenarNome();

	public void ordenarTitulo();

	public void ordenarGenero();

	public void ordenarAno();

	public String consultar(String titulo);

	public String exibir();

}
