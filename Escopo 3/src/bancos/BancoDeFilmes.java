package bancos;

import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Vector;

import models.Filme;

/**
 * Classe que manipula e organiza os filmes
 * 
 * @author Adriano
 *
 */
public class BancoDeFilmes implements BancoDeMidias, Serializable {

	private Vector<Filme> filmes = new Vector<Filme>();

	/**
	 * Metodo que cadastra um filme no banco.
	 * 
	 * @param filme
	 *            filme a ser cadastrado.
	 * 
	 */
	public void cadastrar(Filme filme) {
		filmes.add(filme);

	}

	/**
	 * 
	 * @param titulo
	 * @return
	 */
	
	public boolean excluir(String titulo) {
		for (int j = 0; j < filmes.size(); j++) {
			if (filmes.get(j).getTitulo().equalsIgnoreCase(titulo)) {

				filmes.remove(j);
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param titulo
	 * @param filme
	 * @return
	 */
	public boolean editar(String titulo, Filme filme) {

		for (int i = 0; i < filmes.size(); i++) {
			if (filmes.get(i).getTitulo().equals(titulo)) {

				filmes.set(i, filme);
				return true;

			}

		}
		return false;
	}

	public String listarTitulos() {
		String titulos = "";
		for (int i = 0; i < filmes.size(); i++) {
			titulos += (filmes.get(i).getTitulo() + "\n");
		}
		return titulos;

	}

	/**
	 * 
	 * @return
	 */

	public String listarFilmes() {
		if (filmes.size() > 0) {
			String str = "";

			for (int j = 0; j < filmes.size(); j++) {
				str += " \n";
				str += filmes.get(j).toString();
				str += "\n";

			}
			return str;

		}

		return null;
	}

	/**
	 * 
	 * @param titulo
	 */

	public void remover(String titulo) {
		for (int i = 0; i < filmes.size(); i++) {
			if (filmes.get(i).getTitulo().equalsIgnoreCase(titulo)) {
				filmes.remove(i);
			}
		}
	}

	/**
	 * 
	 * @param titulo
	 * @return
	 */
	public boolean existe(String titulo) {
		for (int i = 0; i < filmes.size(); i++) {
			if (filmes.get(i).getTitulo().equalsIgnoreCase(titulo)) {

				return true;
			}

		}
		return false;

	}

	/**
	 * M�todos para ordena��o
	 * 
	 * Titulo G�nero Ano
	 * 
	 */

	public void ordenarTitulo() {

		Filme aux;
		for (int i = 0; i < filmes.size(); i++) {
			for (int j = 0; j < filmes.size() - 1; j++) {
				if (filmes.get(j).getTitulo().compareToIgnoreCase(filmes.get(j + 1).getTitulo()) > 0) {
					aux = filmes.get(j);
					filmes.set(j, filmes.get(j + 1));
					filmes.set(j + 1, aux);
				}
			}
		}
	}

	public void ordenarGenero() {
		Filme aux;
		for (int i = 0; i < filmes.size(); i++) {
			for (int j = 0; j < filmes.size() - 1; j++) {
				if (filmes.get(j).getGenero().compareToIgnoreCase(filmes.get(j + 1).getGenero()) > 0) {
					aux = filmes.get(j);
					filmes.set(j, filmes.get(j + 1));
					filmes.set(j + 1, aux);
				}
			}
		}
	}

	public void ordenarAno() {
		Filme aux;
		for (int i = 0; i < filmes.size(); i++) {
			for (int j = 0; j < filmes.size() - 1; j++) {
				if (filmes.get(j).getAno() > filmes.get(j + 1).getAno()) {
					aux = filmes.get(j);
					filmes.set(j, filmes.get(j + 1));
					filmes.set(j + 1, aux);
				}
			}
		}
	}

	/**
	 * M�todos de Busca
	 * 
	 * G�nero Ano Diretor
	 * 
	 */

	public String buscaGenero(String genero) {
		String str="";
		for (int i = 0; i < filmes.size(); i++) {
			if (filmes.get(i).getGenero().equals(genero)) {
				str += "Nome do Arquivo: " + filmes.get(i).getNomeDoArquivo() + "\n";
				str += "Titulo: " + filmes.get(i).getTitulo() + "\n";
				str += "Descri��o: " + filmes.get(i).getDescricao() + "\n";
				str += "G�nero: " + filmes.get(i).getGenero() + "\n";
				str += "Idioma: " + filmes.get(i).getIdioma() + "\n";
				str += "Diretor: " + filmes.get(i).getDiretor() + "\n";
				str += "Atores Principais: " + filmes.get(i).getAtoresPrincipais() + "\n";
				str += "Dura��o: " + filmes.get(i).getDuracao() + "\n";
				str += "Ano: " + filmes.get(i).getAno() + "\n";
				str += "\n\n";
			}

		}
		return str;
	}

	public String buscaAno(int ano) {
		String str="";
		for (int i = 0; i < filmes.size(); i++) {
			if (filmes.get(i).getAno() == ano) {

				str += "Nome do Arquivo: " + filmes.get(i).getNomeDoArquivo() + "\n";
				str += "Titulo: " + filmes.get(i).getTitulo() + "\n";
				str += "Descri��o: " + filmes.get(i).getDescricao() + "\n";
				str += "G�nero: " + filmes.get(i).getGenero() + "\n";
				str += "Idioma: " + filmes.get(i).getIdioma() + "\n";
				str += "Diretor: " + filmes.get(i).getDiretor() + "\n";
				str += "Atores Principais: " + filmes.get(i).getAtoresPrincipais() + "\n";
				str += "Dura��o: " + filmes.get(i).getDuracao() + "\n";
				str += "Ano: " + filmes.get(i).getAno() + "\n";
				str += "\n\n";
			}
		}
		return str;
	}

	public String buscaDiretor(String diretor) {
		String str="";
		for (int i = 0; i < filmes.size(); i++) {

			if (filmes.get(i).getDiretor().equals(diretor)) {
				str += "Nome do Arquivo: " + filmes.get(i).getNomeDoArquivo() + "\n";
				str += "Titulo: " + filmes.get(i).getTitulo() + "\n";
				str += "Descri��o: " + filmes.get(i).getDescricao() + "\n";
				str += "G�nero: " + filmes.get(i).getGenero() + "\n";
				str += "Idioma: " + filmes.get(i).getIdioma() + "\n";
				str += "Diretor: " + filmes.get(i).getDiretor() + "\n";
				str += "Atores Principais: " + filmes.get(i).getAtoresPrincipais() + "\n";
				str += "Dura��o: " + filmes.get(i).getDuracao() + "\n";
				str += "Ano: " + filmes.get(i).getAno() + "\n";

				
			}

		}
		return str;
	}

	/**
	 * Le o conjunto de Filmes de um arquivo, cujo nome � informado por
	 * par�metro.
	 * 
	 * @param nomeArquivo
	 *            Nome do arquivo onde as informa��es do conjunto de Filmes
	 *            ser�o gravadas.
	 * @throws ClassNotFoundException
	 * @throws Exception
	 *             lan�a interrup��o referente a manipula��o com arquivos.
	 */

	@SuppressWarnings("unchecked")
	public void le(String nomeDoArquivo) throws IOException, ClassNotFoundException {

		ObjectInputStream obj;

		// abre o arquivo para leitura

		obj = new ObjectInputStream(new FileInputStream(new File(nomeDoArquivo)));

		// le o nome do filme

		filmes = (Vector<Filme>) obj.readObject();

		obj.close();

	}

	/**
	 * Grava no arquivo, cujo nome � informado por par�metro, todas informa��es
	 * do filme.
	 * 
	 * @param nomeDoArquivo
	 *            Nome do arquivo onde as informa��es do filme ser�o gravadas.
	 * @throws Exception
	 *             retorna a exce��o caso ocorra
	 */
	public void escreve(String nomeDoArquivo) throws IOException {

		ObjectOutputStream saida;

		// abre o arquivo para escrita
		saida = new ObjectOutputStream(new FileOutputStream(new File(nomeDoArquivo)));

		// grava o nome do filme
		saida.writeObject(filmes);

		// fecha o arquivo
		saida.close();
	}
	
	public int validaInt(String mensagem) {
		
		Scanner entrada = new Scanner(System.in);
		
		while (true) {
			
			System.out.print(mensagem);
			try {

				int i = entrada.nextInt();
				
				if(i < 0){
					throw new IllegalArgumentException();
				}
				
				return i;

			} catch (InputMismatchException e) {
				System.out.println("Voc� n�o digitou um caractere v�lido!!!");
				entrada.nextLine();

			} catch (IllegalArgumentException ee){
	            System.out.println("Voc� digitou um n�mero negativo, favor digitar novamente!!!");
	            entrada.nextLine();
			}
		}

	}

	public String validaString(String mensagem) {
		Scanner entrada = new Scanner(System.in);
		String str = new String("");

		while (true) {

			System.out.print(mensagem);
			str = entrada.nextLine();

			if (str.isEmpty()) {

				System.out.println("Este campo n�o pode ficar em branco");
				entrada.nextLine();

			}else{
			return str;
			}
		}

	}
	
	public int validaAno(String mensagem) {
		
		Scanner entrada = new Scanner(System.in);
		
		while (true) {
			
			System.out.print(mensagem);
			try {

				int i = entrada.nextInt();
		
				// Tratamento para o usu�rio n�o digitar ano inferior � 1895 que foi o ano 
				// primerio filme da hist�ria.
				if(i < 1895 ){
					throw new IllegalArgumentException();
				// Tratamento para o usu�rio n�o digitar ano superior ao atual.	
				}else if (i > 2015){
					throw new IllegalArgumentException();
				}
				
				return i;

			} catch (InputMismatchException e) {
				System.out.println("Voc� n�o digitou um n�mero v�lido!!!");
				entrada.nextLine();

			} catch (IllegalArgumentException ee){
	            System.out.println("Voc� digitou um ano de filme inv�lido!!!");
	            entrada.nextLine();
			}
		}

	}
	

	@Override
	public void le() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void escreve() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ordenarNome() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String consultar(String titulo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String exibir() {
		// TODO Auto-generated method stub
		return null;
	}

}
