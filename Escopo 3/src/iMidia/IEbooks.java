package iMidia;

import java.io.*;
import java.util.Scanner;

import models.EBook;
import bancos.BancoDeEbooks;

/**
 * Classe interface com o usuario, permmite cadastro, exibi��o, consulta,
 * edi��o, exclus�o dos dados da midia Ebook.
 * 
 * @author Lincoln Pereira
 *
 */

public class IEbooks extends BancoDeEbooks {

	BancoDeEbooks ebooks = new BancoDeEbooks();

	/**
	 * M�todo para exibir o menu para o usuario.
	 * 
	 */

	public IEbooks() {

				
		EBook StarWars = new EBook("C:/midia/ebooks/StarWars.pdf",
				"Star Wars Ep. VI", "Melhor ebook fic��o cient�fica ",
				"Aventura", "Kahn, James ", 659, "LUCASBOOKS ", 2011, "EUA ");

		EBook SherlockHolmes = new EBook("C:/midia/ebooks/SherlockHolmes.pdf",
				"Sherlock Holmes", "Ebook Premiado de Aventura ", "Aventura",
				"Doyle, Arthur ", 218, "Strand Magazine ", 2002, "Inglaterra ");

		EBook Persuasao = new EBook("C:/midia/ebooks/Persuasao.pdf",
				"Persuas�o", "Obra P�stuma de 1818 ", "Romance",
				"Austen, Jane ", 125, "Northanger ", 2012, "EUA ");
		EBook AvoltaAoMundoEmOitentaDias = new EBook(
				"C:/midia/ebooks/AvoltaAoMundoEmOitentaDias.pdf",
				"A Volta ao Mundo em 80 Dias", "Cl�ssico ", "Aventura",
				"Verne, Julio ", 236, "Kindle ", 2004, "EUA ");
		EBook OrgulhoPreconceito = new EBook(
				"C:/midia/ebooks/OrgulhoePreconceito.pdf",
				"Orgulho e Preconceito", "Drama do S�culo XVIII ", "Romance",
				"Austen, Jane ", 392, "Landmark ", 2012, "EUA ");
		EBook Dracula = new EBook("C:/midia/ebooks/Dracula.pdf", "Dr�cula",
				"Cl�ssico do Terror ", "Terror", "Stoker, Bram ", 842,
				"Landmark ", 2010, "Inglaterra ");
		EBook MobyDick = new EBook("C:/midia/ebooks/MobyDick.pdf", "Moby Dick",
				"Cl�ssico de Aventura ", "Aventura", "Mellville, Herman ",
				1245, "Landmark ", 2013, "EUA ");
		EBook Judas = new EBook("C:/midia/ebooks/Judas.pdf", "Judas",
				"Ebook de Hist�ria ", "�pico", "Oz, Am�s ", 299,
				"Companhia das Letras ", 2014, "Brasil ");
		EBook Emma = new EBook("C:/midia/ebooks/Emma.pdf", "Emma",
				"Ebook de Romance Contempor�neo ", "Com�da Rom�ntica",
				"Austen, Jane ", 888, "Landmark ", 2012, "EUA ");
		EBook Inferno = new EBook("C:/midia/ebooks/Inferno.pdf", "Inferno",
				"Cl�ssico Medieval ", "�pico", "Alighieri, Dante ", 234,
				"Amazon ", 2011, "EUA ");
		
		ebooks.cadastrar(StarWars);
		ebooks.cadastrar(SherlockHolmes);
		ebooks.cadastrar(Persuasao);
		ebooks.cadastrar(AvoltaAoMundoEmOitentaDias);
		ebooks.cadastrar(OrgulhoPreconceito);
		ebooks.cadastrar(Dracula);
		ebooks.cadastrar(MobyDick);
		ebooks.cadastrar(Judas);
		ebooks.cadastrar(Emma);
		ebooks.cadastrar(Inferno);

	}

	public void menuEBooks() {

		Scanner entrada = new Scanner(System.in);

	//	try {
    //  	ebooks.le("Ebook.bin");

	//	} catch (Exception e) {

	//	}

		int opcao;

		do {
			System.out.println("*****************************");
			System.out.println("\t MENU DE EBOOKS");
			System.out.println("*****************************");
			System.out.println("1. CADASTRAR");
			System.out.println("2. LISTAR");
			System.out.println("3. CONSULTAR");
			System.out.println("4. EDITAR");
			System.out.println("5. EXCLUIR");
			System.out.println("6. ORDENAR");
			System.out.println("0. SAIR");

			opcao = ebooks.validaInt("Escolha a opcao (0 a 5): ");

			if (opcao == 1) {
				cadastrarEBook();

			} else if (opcao == 2) {
				consultarEBook();

			} else if (opcao == 3) {
				do {
					System.out.println("*****************************");
					System.out.println("\t MENU PARA BUSCA");
					System.out.println("*****************************");
					System.out.println("1. Busca por Autores");
					System.out.println("2. Busca por Local");
					System.out.println("3. Busca por Editora");
					System.out.println("0. Voltar ao Menu Principal");

					opcao = ebooks.validaInt("Escolha a opcao (0 a 3):");

					if (opcao == 1) {

						System.out
								.println("Informe o Autor do Ebook que deseja Recuperar: ");
						String resposta = entrada.nextLine();
						String str = ebooks.buscaAutores(resposta);
						if (str != null) {
							System.out.println(" --------- EBOOKS DO AUTOR "
									+ resposta + "  --------- \n\n");

							System.out.println(str);
						} else {
							System.out
									.println("N�o existe ebook cadastrado desse Autor ! \n\n");
						}

					} else if (opcao == 2) {
						System.out
								.println("Informe o Local do Ebook que deseja Recuperar: ");
						String resposta = entrada.nextLine();
						String str = ebooks.buscaLocal(resposta);

						if (str != null) {
							System.out.println(" --------- EBOOKS DO LOCAL "
									+ resposta + "  --------- \n\n");

							System.out.println(str);
						} else {
							System.out
									.println("N�o existe ebook cadastrado desse local ! \n\n");
						}

					} else if (opcao == 3) {
						System.out
								.println("Informe a Editora do ebook que deseja Recuperar: ");
						int res = entrada.nextInt();
						String str = ebooks.buscaAno(res);
						System.out.println("\n\n");

						if (str != null) {
							System.out.println(" --------- ebooks DO ANO "
									+ res + "  --------- \n\n");
							System.out.println(ebooks.buscaAno(res) + "\n\n");
						} else {
							System.out
									.println("Volte ao menu principal" + "\n");
						}

					}

				} while (opcao != 0);
				if (opcao == 0) {
					menuEBooks();

				}

			} else if (opcao == 4) {
				editarEbook();

			} else if (opcao == 5) {
				excluirEBook();

			} else if (opcao == 6)

				do {
					System.out.println("*****************************");
					System.out.println("\t MENU DE ORDENA��O");
					System.out.println("*****************************");
					System.out.println("1. Ordenar por Titulo");
					System.out.println("2. Ordenar por Genero");
					System.out.println("3. Ordenar por Ano");
					System.out.println("0. Voltar ao Menu Principal");

					opcao = ebooks.validaInt("Escolha a opcao (0 a 3):");

					if (opcao == 1) {
						ebooks.ordenarTitulo();
						consultarEBook();
					} else if (opcao == 2) {
						ebooks.ordenarGenero();
						consultarEBook();
					} else if (opcao == 3) {
						ebooks.ordenarAno();
						consultarEBook();
					}
					if (opcao == 0) {
						menuEBooks();

					}

				} while (opcao != 0);

		} while (opcao != 0);

		try {
			ebooks.escreve("Ebook.bin");
		} catch (FileNotFoundException ex) {
			System.err.println("Erro: Ebook.bin n�o encontrado");
		} catch (Exception e) {
			System.err.println("Erro: " + e.getMessage());

		}
	}

	private void consultarEBook() {
		Scanner entrada = new Scanner(System.in);
		if (ebooks.listarEBooks() != null) {
			System.out.println(ebooks.listarEBooks());

			System.out.println("\n\n");
		} else {
			System.out.println("Nenhum ebook encontrado !");
			System.out.println("Cadastrar um ebook ? S/N");
			String resposta = entrada.nextLine();

			if (resposta.equalsIgnoreCase("s")) {

				cadastrarEBook();
			}
		}

	}

	/**
	 * M�todo para adicionar um Ebook
	 */
	private void cadastrarEBook() {

		Scanner entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		int ano;
		String autores;
		String local;
		String editora;
		int nPaginas;

		System.out.println("*****************************");
		System.out.println("\tCadastrar ebook");
		System.out.println("*****************************");
		nomeDoArquivo = ebooks.validaString("Informe o diret�rio do ebook: ");
		titulo = ebooks.validaString("Informe o titulo do ebook: ");
		descricao = ebooks.validaString("Informe a descri��o do ebook: ");
		genero = ebooks.validaString("Informe o genero do ebook: ");
		ano = ebooks.validaInt("Informe o ano do ebook: ");
		autores = ebooks.validaString("Informe os autores do ebook: ");
		local = ebooks.validaString("Informe o local de origem do ebook: ");
		editora = ebooks.validaString("Informe a Editora: ");

		nPaginas = ebooks.validaInt("Informe o n�mero de p�ginas do ebook: ");

		EBook f = new EBook(nomeDoArquivo, titulo, descricao, genero, autores,
				nPaginas, editora, ano, local);
		ebooks.cadastrar(f);
	}

	/**
	 * M�todo para listar os filmes do ArrayList, caso nao exista nenhum ebook
	 * cadastrado � dado ao usuario a opcao de cadastra-lo.
	 */

	private void exibirTitulos() {

		System.out.println(ebooks.listarTitulos());

	}

	/**
	 * M�todo de edicao de um ebook especifico
	 */

	private void editarEbook() {

		Scanner entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		int ano;
		String autores;
		String local;
		String editora;
		int nPaginas;
		String resposta;

		System.out.println("Informe o T�tulo do Ebook que deseja alterar: ");
		resposta = entrada.nextLine();

		if (ebooks.existe(resposta)) {

			System.out.println("*****************************");
			System.out.println("\tEditar Ebook");
			System.out.println("*****************************");
			nomeDoArquivo = ebooks
					.validaString("Informe o diret�rio do ebook: ");
			titulo = ebooks.validaString("Informe o titulo do ebook: ");
			descricao = ebooks.validaString("Informe a descri��o do ebook: ");
			genero = ebooks.validaString("Informe o genero do ebook: ");
			ano = ebooks.validaInt("Informe o ano do filme: ");
			autores = ebooks.validaString("Informe os autores do ebook: ");
			local = ebooks.validaString("Informe o local de origem do ebook: ");
			editora = ebooks
					.validaString("Informe a editora do ebook: ");
			nPaginas = ebooks.validaInt("Informe o n�mero de p�ginas do ebook: ");

			EBook ebook = new EBook(nomeDoArquivo, titulo, descricao, genero,
					autores, nPaginas, editora, ano, local);

			if (ebooks.editar(resposta, ebook)) {
				System.out.println("Ebook alterado com sucesso.");
			} else {
				System.out.println("Ebook n�o encontrado");
			}
		}
	}

	/**
	 * M�todo para excluir um ebook.
	 */

	private void excluirEBook() {
		Scanner entrada = new Scanner(System.in);
		String titulo;
		System.out.println("*****************************");
		System.out.println("\tExcluir Ebook ");
		System.out.println("*****************************");
		titulo = ebooks.validaString("Informe o T�tulo do ebook");

		if (ebooks.excluirTitulo(titulo)) {
			System.out.println("Ebook removido com sucesso!");
		} else {
			System.out.println("Ebook n�o encontrado!");
		}

	}
}
