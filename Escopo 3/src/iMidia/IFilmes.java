package iMidia;

import java.io.*;
import java.util.Scanner;

import models.Filme;
import bancos.BancoDeFilmes;

/**
 * Classe que realiza a interface com o usuario, permetindoo cadastro, exibi��o,
 * consulta, edi��o, exclus�o dos dados da midia filme.
 * 
 * @author Adriano
 *
 */

public class IFilmes extends BancoDeFilmes {
	
	

	BancoDeFilmes filmes = new BancoDeFilmes();

	// private Scanner menu;

	/**
	 * M�todo que exibe o menu com as op��es para o usuario.
	 * 
	 */
	
	public IFilmes() {
		
		Filme hercules = new
				 Filme("C:/midia/filmes/As aventuras de hercules", "Hercules",
				 "Heroica aventura de Hercules, filho de Zeus", "Fantasia", "Ingles",
				 "Brett Ratner", "Dwayne Johnson, Rufus Sewell", 98, 2014);
				
				 Filme bones = new Filme("C:/midia/filmes/the bones", "bones",
				 "investiga�ao policial", "A�ao", "Ingles", "Bratt Tompson",
				 "Emely Deschanel, David Boreanaz", 43, 2010);
				
				 Filme mercenarios = new Filme("C:/midia/filmes/Os mercenarios",
				 "mercenarios", "descricao", "policial", "Ingles",
				 "Sylvester Stallone", "Sylvester Stallone, Jet Li", 100, 2010);
				
				 Filme vingadores = new Filme("C:/midia/filmes/the Avengers",
				 "Os Vingadores", "Aventura dos quadrinhos Marvel",
				 "Fic�ao cientifica", "Portugues", "Joss Whedon",
				 "Robert Downey Jr, Chris Evans", 140, 2012);
				
				 Filme hobbit = new Filme("C:/midia/filmes/O Hobbit",
				 "O Hobbit, a batalha dos cinco exercitos ",
				 "Apos ser expulso de Erebor, o dragao Smaug ataca a cidade dos homens",
				 "Fantasia", "Ingles", "Peter Jackson",
				 "Martin Freeman, Richard Armitage", 144, 2014);
				
				 Filme sebeber = new Filme("C:/midia/filmes/Se beber não case",
				 "Se beber, nao case",
				 "Trio de amigos organiza uma despedida de solteiro", "Comedia",
				 "Portugues", "Todd Phillips", "Bradley Cooper, Ed Helms", 90, 2009);
				
				 Filme eradogelo = new Filme("C:/midia/filmes/Era do gelo",
				 "A era do gelo 4",
				 "O esquilo Scrat provoca a separa��o dos continentes",
				 "Comedia infantil", "Portugues", "Steve Martino",
				 "Ray Romano, Denis Leary", 94, 2012);
				
				 Filme debiloide = new Filme("C:/midia/filmes/Debi e Loide ",
				 "Debi & Loide 2",
				 "Mais uma nova aventura de Lloyd Christmas e Harry Dunne", "Comedia",
				 "Portugues", "Bobby Farrelly", "Jim Carrey, Jeff Daniels", 104,
				 2014);
				
				 Filme exodo = new Filme("C:/midia/filmes/O exodo ",
				 "O exodo, Deuses e Reis", "Adapta��o hist�rica b�blica do exodo",
				 "Epico", "Ingles", "Ridley Scott", "Christian Bale, John Turturro",
				 151, 2014);
				
				 Filme reiarthur = new Filme("C:/midia/filmes/Rei Arthur ",
				 "O rei Arthur", "Arthur � um lider que deseja libertar a Bretanha",
				 "Epico", "Ingles", "Antoine Fuqua", "Clive Owen, Keira Knightley",
				 166, 2004);
				
				 filmes.cadastrar(hercules);
				 filmes.cadastrar(bones);
				 filmes.cadastrar(mercenarios);
				 filmes.cadastrar(vingadores);
				 filmes.cadastrar(hobbit);
				 filmes.cadastrar(sebeber);
				 filmes.cadastrar(eradogelo);
				 filmes.cadastrar(debiloide);
				 filmes.cadastrar(exodo);
				 filmes.cadastrar(reiarthur);
		
	}
	public void menuFilmes() {

		Scanner entrada = new Scanner(System.in);
		 
		 try {
				filmes.le("Filme.bin");
			
			
			}catch (Exception e) {
				
			}
		 

		int opcao;

		do {
			System.out.println("*****************************");
			System.out.println("\t MENU DE FILMES");
			System.out.println("*****************************");
			System.out.println("1. CADASTRAR");
			System.out.println("2. LISTAR TODOS");
			System.out.println("3. CONSULTAR");
			System.out.println("4. EDITAR");
			System.out.println("5. EXCLUIR");
			System.out.println("6. ORDENAR");
			System.out.println("0. Sair");

			opcao = filmes.validaInt("Escolha a opcao (0 a 6): ");

			if (opcao == 1) {
				cadastrarFilme();
				
			} else if (opcao == 2) {
				consultarFilme();
				

			} else if (opcao == 3) {
				do {
					System.out.println("*****************************");
					System.out.println("\t MENU DE CONSULTA");
					System.out.println("*****************************");
					System.out.println("1. Busca por Diretor");
					System.out.println("2. Busca por G�nero");
					System.out.println("3. Busca por Ano");
					System.out.println("0. Voltar ao Menu Principal");

					opcao = filmes.validaInt("Escolha a opcao (0 a 3):");

					if (opcao == 1) {

						System.out.println("Informe o Diretor do Filme que deseja Recuperar: ");
						String resposta = entrada.nextLine();
						String str = filmes.buscaDiretor(resposta);
						if( str != null){
							System.out.println(" --------- FILMES DO DIRETOR " + resposta + "  --------- \n\n");

							System.out.println(str);
						}else{
								System.out.println("N�o existe filme cadastrado com esse Diretor ! \n\n");
						}

					} else if (opcao == 2) {
						System.out.println("Informe o G�nero do Filme que deseja Recuperar: ");
						String resposta = entrada.nextLine();
						String str = filmes.buscaGenero(resposta);
						
						if( str != null){
							System.out.println(" --------- FILMES DO G�NERO " + resposta + "  --------- \n\n");

							System.out.println(str);
						}else{
							System.out.println("N�o existe filme cadastrado com esse g�nero ! \n\n");
						}

					} else if (opcao == 3) {
						System.out.println("Informe o Ano do Filme que deseja Recuperar: ");
						int res = entrada.nextInt();
						String str = filmes.buscaAno(res);
						System.out.println("\n\n");
						
						if( str != null){
						System.out.println(" --------- FILMES DO ANO " + res + "  --------- \n\n");
						System.out.println(filmes.buscaAno(res)+"\n\n");
						}else{
							System.out.println("Volte ao menu principal"+"\n");
						}
					
					} else if (opcao >= 4){
						System.out.println("Digite apenas as op��es do Menu de 1 a 3 ou 0 para voltar ao menu principal!!!"+"\n\n");
					}	

				} while (opcao != 0);
				if(opcao == 0){
					menuFilmes();
					
				}

			
			} else if (opcao == 4) {
				editarFilme();
			
			} else if (opcao == 5) {
				excluirFilme();

			} else if (opcao == 6) {

				do {
					System.out.println("*****************************");
					System.out.println("\t MENU DE ORDENA��O");
					System.out.println("*****************************");
					System.out.println("1. Ordenar por Titulo");
					System.out.println("2. Ordenar por Genero");
					System.out.println("3. Ordenar por Ano");
					System.out.println("0. Voltar ao Menu Principal");

					opcao = filmes.validaInt("Escolha a opcao (0 a 3):"+"\n\n");

					if (opcao == 1) {
						filmes.ordenarTitulo();
						consultarFilme();
					} else if (opcao == 2) {
						filmes.ordenarGenero();
						consultarFilme();
					} else if (opcao == 3) {
						filmes.ordenarAno();
						consultarFilme();
					} else if (opcao >= 4){
						System.out.println("Digite apenas as op��es do Menu de 1 a 3 ou 0 para voltar ao menu principal!!!"+"\n\n");
					}
					if(opcao == 0){
						menuFilmes();
						
					}

				} while (opcao != 0);
			
			} else if (opcao >= 7){
				
				System.out.println("Digite apenas as op��es do Menu de 1 a 6 ou 0 para sair do programa!!!"+"\n\n");
			}
			

		} while (opcao != 0);
			System.out.println("Sistema Finalizado com sucesso!!!");

		try {
			filmes.escreve("Filme.bin");
		} catch (FileNotFoundException ex) {
			System.err.println("Erro: Filmes.bin n�o encontrado");
		} catch (Exception e) {
			System.err.println("Erro: " + e.getMessage());

		}
	}

	private void consultarFilme() {
		Scanner entrada = new Scanner(System.in);
		if (filmes.listarFilmes() != null) {
			System.out.println(filmes.listarFilmes());

			System.out.println("\n\n");
		} else {
			System.out.println("Nenhum Filme encontrado !");
			System.out.println("Deseja cadastrar um Filme ? S/N");
			String resposta = entrada.nextLine();

			if (resposta.equalsIgnoreCase("s")) {

				cadastrarFilme();
			}
		}

	}

	/**
	 * M�todo para adicionar um novo filme
	 */
	private void cadastrarFilme() {

		Scanner entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		String idioma;
		String diretor;
		String atoresPrincipais;
		int duracao;
		int ano;
		int resposta;
		System.out.println("*****************************");
		System.out.println("\tCadastrar Filme");
		System.out.println("*****************************");
		nomeDoArquivo = filmes.validaString("Informe o diret�rio do filme: ");
		titulo = filmes.validaString("Informe o titulo do filme: ");
		descricao = filmes.validaString("Informe a descri��o do filme: ");
		genero = filmes.validaString("Informe o genero do filme: ");
		idioma = filmes.validaString("Informe o idioma do filme: ");
		diretor = filmes.validaString("Informe o diretor do filme: ");
		atoresPrincipais = filmes.validaString("Informe o atores principais do filme: ");
		duracao = filmes.validaInt("Informe a dura��o do filme: ");
		ano = filmes.validaAno("Informe o ano do filme: ");
		
		Filme f = new Filme(nomeDoArquivo, titulo, descricao, genero, idioma,
				diretor, atoresPrincipais, duracao, ano);
		filmes.cadastrar(f);
	}

	/**
	 * M�todo para listar os filmes do ArrayList, caso nao exista nenhum filme
	 * cadastrado � dado ao usuario a opcao de cadastra-lo.
	 */

	private void exibirTitulos() {

		System.out.println(filmes.listarTitulos());

	}

	/**
	 * M�todo de edicao de um filme especifico
	 */

	private void editarFilme() {

		Scanner entrada = new Scanner(System.in);

		String nomeDoArquivo;
		String titulo;
		String descricao;
		String genero;
		String idioma;
		String diretor;
		String atoresPrincipais;
		int duracao;
		int ano;
		boolean existe = true;
		String resposta;

		System.out.println("Informe o T�tulo do Filme que deseja alterar: ");
		resposta = entrada.nextLine();

		if (filmes.existe(resposta)) {

			System.out.println("*****************************");
			System.out.println("\tEditar Filme");
			System.out.println("*****************************");
			nomeDoArquivo = filmes.validaString("Informe o diret�rio do filme: ");
			titulo = filmes.validaString("Informe o titulo do filme: ");
			descricao = filmes.validaString("Informe a descri��o do filme: ");
			genero = filmes.validaString("Informe o genero do filme: ");
			idioma = filmes.validaString("Informe o idioma do filme: ");
			diretor = filmes.validaString("Informe o diretor do filme: ");
			atoresPrincipais = filmes.validaString("Informe o atores principais do filme: ");
			duracao = filmes.validaInt("Informe a dura��o do filme: ");
			ano = filmes.validaInt("Informe o ano do filme: ");
			

			Filme filme = new Filme(nomeDoArquivo, titulo, descricao, genero,
					idioma, diretor, atoresPrincipais, duracao, ano);

			if (filmes.editar(resposta, filme)) {
				System.out.println("O filme foi alterada com sucesso.");
			} else {
				System.out.println("O filme nao foi encontrada");
			}
		}

	}

	/**
	 * M�todo para excluir um filme espec�fico.
	 */

	private void excluirFilme() {
		Scanner entrada = new Scanner(System.in);
		String titulo;
		System.out.println("*****************************");
		System.out.println("\tExcluir Filme ");
		System.out.println("*****************************");
		titulo = filmes.validaString("Informe o T�tulo do filme: ");
		
		if (filmes.excluir(titulo)) {
			System.out.println("Filme removido com sucesso!"+"\n\n");
		} else {
			System.out.println("Filme n�o encontrado!"+"\n\n");
		}

	}

}