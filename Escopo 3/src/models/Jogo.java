package models;

/**
 * Classe que representa um tipo de Midia, no caso um Jogo.
 * 
 * @author victor costa
 *
 */
public class Jogo extends Midia implements Comparable<Jogo> {

	/**
	 * Construtor basico da classe.
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo
	 * @param titulo
	 *            o titulo do jogo
	 */
	public Jogo(String nomeDoArquivo, String titulo) {
		super(nomeDoArquivo, titulo);
	}

	/**
	 * Construtor que recebe e define todos os dados do jogo.
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo.
	 * @param titulo
	 *            o titulo do jogo.
	 * @param descricao
	 *            uma descri��o do jogo.
	 * @param genero
	 *            o genero do jogo.
	 * @param autores
	 *            os autores do jogo.
	 * @param numeroDeJogadores
	 *            o numero de jogadores.
	 * @param suporteARede
	 *            se existe ou n�o suporte a rede.
	 * @param ano
	 *            o ano do jogo.
	 */
	public Jogo(String nomeDoArquivo, String titulo, String descricao,
			String genero, String autores, int numeroDeJogadores,
			boolean suporteARede, int ano) {
		this(nomeDoArquivo, titulo);
		this.descricao = descricao;
		this.genero = genero;
		this.ano = ano;
		this.autores = autores;
		this.numeroDeJogadores = numeroDeJogadores;
		this.suporteARede = suporteARede;
	}

	private String autores;

	private int numeroDeJogadores;

	private boolean suporteARede;

	/**
	 * Metodo que redefine os autores do jogo
	 * 
	 * @param autores
	 *            os autores.
	 */
	public void setAutores(String autores) {
		this.autores = autores;
	}

	/**
	 * Metodo que retorna os autores do jogo.
	 * 
	 * @return os autores.
	 */
	public String getAutores() {
		return autores;
	}

	/**
	 * Metodo que redefine o numero de jogadores.
	 * 
	 * @param numeroDeJogadores
	 *            o numero de jogadores.
	 */
	public void setNumeroDeJogadores(int numeroDeJogadores) {
		this.numeroDeJogadores = numeroDeJogadores;
	}

	/**
	 * Metodo que recupera o numero de jogadores.
	 * @return o numero de jogadores.
	 */
	public int getNumeroDeJogadores() {
		return numeroDeJogadores;
	}
/**
 * Metodo que redefine se existe ou n�o suporte a rede.
 * @param suporteARede true se existir suporte a rede e false se n�o existir.
 */
	public void setSuporteARede(boolean suporteARede) {
		this.suporteARede = suporteARede;
	}
/**
 * Metodo que recupera se h� ou n�o suporte a rede.
 * @return true se existe suporte e false se n�o existir.
 */
	public boolean isSuporteARede() {
		return suporteARede;
	}
/**
 * Metodo que concatena e retorna todas as informa�oes do jogo.
 * @return todas as informa�oes.
 */
	public String toString() {
		return nomeDoArquivo + "\n" + titulo + "\n" + descricao + "\n" + genero
				+ "\n" + autores + "\n" + numeroDeJogadores + "\n"
				+ suporteARede + "\n" + ano + "\n\n";

	}

public int compareTo(Jogo o) {
	return this.nomeDoArquivo.compareTo(o.getNomeDoArquivo());
}

}
