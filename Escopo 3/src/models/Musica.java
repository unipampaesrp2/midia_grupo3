package models;

/**
 * Classe que representa um tipo de Midia, no caso uma Musica.
 * 
 * @author Diovane Freitas
 *
 */

public class Musica extends Midia {
	
	/**
	 * Construtor basico da classe.
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo
	 * @param titulo
	 *            o titulo da musica
	 */

	public Musica(String nomeDoArquivo, String titulo) {
		super(nomeDoArquivo, titulo);
	}
	
	/**
	 * Construtor que recebe e define todos os dados da musica.
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo.
	 * @param titulo
	 *            o titulo da musica.
	 * @param descricao
	 *            uma descri��o da musica.
	 * @param genero
	 *            o genero da musica.
	 * @param autores
	 *            os autores da musica.
	 * @param interpretes
	 *            os interpretes da musica.
	 * @param duracao
	 *            a duracao da musica.
	 * @param idioma
	 * 			  o idioma da musica.         
	 * @param ano
	 *            o ano do jogo.
	 */
	public Musica(String nomeDoArquivo, String titulo, String descricao,
			String genero, String autores, String interpretes, int duracao,
			String idioma, int ano) {
		this(nomeDoArquivo, titulo);
		this.autores = autores;
		this.idioma = idioma;
		this.interpretes = interpretes;
		this.duracao = duracao;
		this.ano = ano;
		this.descricao = descricao;
		this.genero = genero;
	}

	private String autores;

	private String idioma;

	private String interpretes;

	private int duracao;

	/**
	 * Metodo que retorna os autores da musica.
	 * 
	 * @return os autores.
	 */
	public String getAutores() {
		return autores;
	}
	
	/**
	 * Metodo que redefine os autores da musica
	 * 
	 * @param autores
	 *            os autores.
	 */
	public void setAutores(String autores) {
		this.autores = autores;
	}

	/**
	 * Metodo que retorna o idioma da musica.
	 * 
	 * @return o idioma.
	 */
	public String getIdioma() {
		return idioma;
	}

	/**
	 * Metodo que redefine o idioma da musica
	 * 
	 * @param idioma
	 *            o idioma.
	 */
	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}
	
	/**
	 * Metodo que retorna os interpretes da musica.
	 * 
	 * @return os interpretes.
	 */
	public String getInterpretes() {
		return interpretes;
	}

	/**
	 * Metodo que redefine os interpretes da musica
	 * 
	 * @param interpretes
	 *            os interpretes.
	 */
	public void setInterpretes(String interpretes) {
		this.interpretes = interpretes;
	}

	/**
	 * Metodo que retorna a dura��o da musica.
	 * 
	 * @return a dura��o.
	 */
	public int getDuracao() {
		return duracao;
	}

	/**
	 * Metodo que redefine a dura��o da musica
	 * 
	 * @param duracao a duracao
	 */
	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	/**
	 * Metodo que retorna o ano da musica.
	 * 
	 * @return o ano.
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * Metodo que redefine o ano da musica
	 * 
	 * @param ano
	 *            o ano.
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}

	/**
	 * Metodo que concatena e retorna todas as informa�oes da musica.
	 * @return todas as informa�oes.
	 */
	public String toString() {
		return nomeDoArquivo + "\n" + titulo + "\n" + descricao + "\n" + genero
				+ "\n" + autores + "\n" + interpretes + "\n" + duracao + "\n"
				+ idioma + "\n" + ano + "\n\n";
	}

}
