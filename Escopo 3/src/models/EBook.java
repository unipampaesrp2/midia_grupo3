package models;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * Classe que representa o tipo de midia Ebook.
 * 
 * @author lincoln
 *
 */
public class EBook extends Midia implements Serializable {
	private String autores;

	private String local;

	private String editora;

	private int numeroDePaginas;

	/**
	 * Construtor basico da classe
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo
	 * @param titulo
	 *            o titulo do ebook
	 */
	public EBook(String nomeDoArquivo, String titulo) {
		super(nomeDoArquivo, titulo);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Construtor que recebe todas as informa�oes do arquivo
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo
	 * @param titulo
	 *            o titulo
	 * @param descricao
	 *            a descri��o
	 * @param genero
	 *            o genero
	 * @param autores
	 *            os autores
	 * @param numeroDePaginas
	 *            o numero de paginas
	 * @param editora
	 *            a editora
	 * @param ano
	 *            o ano
	 * @param local
	 *            o local de edi�ao
	 */
	public EBook(String nomeDoArquivo, String titulo, String descricao,
			String genero, String autores, int numeroDePaginas, String editora,
			int ano, String local) {
		this(nomeDoArquivo, titulo);
		this.autores = autores;
		this.descricao = descricao;
		this.genero = genero;
		this.ano = ano;
		this.numeroDePaginas = numeroDePaginas;
		this.local = local;
		this.editora = editora;
	}

	/**
	 * Recupera os autores do ebook
	 * 
	 * @return os autores
	 */
	public String getAutores() {
		return autores;
	}

	/**
	 * Metodo que define os autores
	 * 
	 * @param autores
	 *            os novos autores
	 */
	public void setAutores(String autores) {
		this.autores = autores;
	}

	/**
	 * Metodo que recupera o local de edi�ao
	 * 
	 * @return o local
	 */
	public String getLocal() {
		return local;
	}

	/**
	 * Metodo que redefine o local
	 * 
	 * @param local
	 *            o local
	 */
	public void setLocal(String local) {
		this.local = local;
	}

	/**
	 * Metodo que recupera a editora do ebook
	 * 
	 * @return a editora
	 */
	public String getEditora() {
		return editora;
	}

	/**
	 * Metodo que redefine a editora
	 * 
	 * @param editora
	 *            a editora
	 */
	public void setEditora(String editora) {
		this.editora = editora;
	}

	/**
	 * Metodo que retorna o numero de paginas
	 * 
	 * @return o numero de paginas
	 */
	public int getNumeroDePaginas() {
		return numeroDePaginas;
	}

	/**
	 * Metodo que redefine o numero de paginas
	 * 
	 * @param numeroDePaginas
	 *            o numero de paginas
	 */
	public void setNumeroDePaginas(int numeroDePaginas) {
		this.numeroDePaginas = numeroDePaginas;
	}

	/**
	 * Metodo que retorna uma string com todas informa�oes do objeto
	 * 
	 * @return todas info
	 */
	@Override
	public String toString() {
		return nomeDoArquivo + "\n" + titulo + "\n" + descricao + "\n" + genero
				+ "\n" + autores + "\n" + numeroDePaginas + "\n" + editora
				+ "\n" + ano + "\n";

	}

	public void escreveBin(List<EBook> ebooks) throws IOException {
		/**
		 * //Escrita de objeto FileOutputStream fos = new
		 * FileOutputStream("paises.ser"); ObjectOutputStream oos = new
		 * ObjectOutputStream(fos); oos.writeObject(paises); oos.close();
		 */

		// Escrita de objeto
		// FileOutputStream doc = new FileOutputStream(new File("EBook.bin"));

		// abre o arquivo para escrita
		// ObjectOutputStream output = new ObjectOutputStream( new
		// FileOutputStream(new File("EBook.bin")));
		// output.writeObject(ebooks);
		// output.close();
		// grava o nome do Arquivo
		// output.writeObject(nomeDoArquivo);
		// grava o T�tulo
		// output.writeObject(titulo);
		// grava a Descri��o
		// output.writeObject(descricao);
		// grava o Genero
		// output.writeObject(genero);
		// grava os autores
		// output.writeObject(autores);
		// grava o n�mero de p�ginas
		// output.writeObject(numeroDePaginas);
		// grava a editora
		// output.writeObject(editora);
		// grava o ano
		// output.writeObject(ano);
		// grava o local
		// output.writeObject(local);
		// fecha o arquivo
		// output.close();

	}

	public void leBin(String nome) throws IOException, ClassNotFoundException {

		// Leitura de objeto
		// Cria um canal de leitura com o arquico EBook.bin criado anteriormente
		// FileInputStream in = new FileInputStream("EBook.bin");
		// Cria uma inst�ncia de InputStream que serve para ler objetos.
		// ObjectInputStream input = new ObjectInputStream(new FileInputStream(
		// new File(nome)));
		// L� um objeto do arquivo paises.ser e faz um cast
		// ebooks =(ArrayList <EBook>) input.readObject();
		// Fecha o canal de leitura.
		// input.close();
		// Imprime no console o conte�do do array.

		// FileInputStream doc = new FileInputStream(new File("EBook.ser"));

		// ObjectInputStream input = new ObjectInputStream(doc);
		// ObjectInputStream input;
		// abre o arquivo para leitura
		// input = new ObjectInputStream(new FileInputStream(new
		// File("EBook.ser")));
		// ebooks = (ArrayList<EBook>) input.readObject();
		// input.close();
		// abre o arquivo para leitura
		// input = new ObjectInputStream(
		// new FileInputStream(new File("EBook.ser")));
		// nomeDoArquivo = (String) input.readObject();
		// titulo = (String) input.readObject();
		// descricao = (String) input.readObject();
		// genero = (String) input.readObject();
		// autores = (String) input.readObject();
		// numeroDePaginas = (int) input.readObject();
		// editora = (String)input.readObject();
		// ano = (int) input.readObject();
		// local = (String) input.readObject();

		/**
		 * //Leitura de objeto FileInputStream fis = new
		 * FileInputStream("paises.ser"); ObjectInputStream ois = new
		 * ObjectInputStream(fis); String[] pais = (String[]) ois.readObject();
		 * ois.close()
		 */

		/**
		 * //le o nome do banco nome = (String) input.readObject(); //le o
		 * numero de conta atual numeroConta = (int) input.readObject(); //le a
		 * rela��o de clientes clientes = (ArrayList<Cliente>)
		 * input.readObject(); //le a rela��o de contas contas =
		 * (ArrayList<Conta>) input.readObject(); //fecha o arquivo
		 * input.close();
		 */

	}

}
