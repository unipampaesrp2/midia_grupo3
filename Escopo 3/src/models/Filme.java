
package models;

import java.io.Serializable;

public class Filme extends Midia{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Filme(String nomeDoArquivo, String titulo) {
		super(nomeDoArquivo, titulo);
		// TODO Auto-generated constructor stub
	}
	
	public Filme(String nomeDoArquivo, String titulo, String descricao, String genero, String idioma, String diretor, String atoresPrincipais, int duracao, int ano){
		this(nomeDoArquivo, titulo);
		this.descricao = descricao;
		this.genero = genero;
		this.idioma = idioma;
		this.diretor = diretor;
		this.atoresPrincipais = atoresPrincipais;
		this.duracao = duracao;
		this.ano = ano;
		
	}

	private String idioma;

	private String diretor;

	private String atoresPrincipais;

	private int duracao;

	public void setIdioma(String idioma) {
		this.idioma = idioma;

	}

	public String getIdioma() {
		return idioma;
	}

	public void setDiretor(String diretor) {
		this.diretor = diretor;

	}

	public String getDiretor() {
		return diretor;
	}

	public void setAtoresPrincipais(String atoresPrincipais) {
		this.atoresPrincipais = atoresPrincipais;

	}

	public String getAtoresPrincipais() {
		return atoresPrincipais;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;

	}

	public int getDuracao() {
		return duracao;
	}

	@Override
	public String toString() {
		
		String str = "";
		
		str += "Nome do Arquivo: " + nomeDoArquivo + "\n";
		str += "Titulo: " + titulo + "\n";
		str += "Descri��o: " + descricao + "\n";
		str += "G�nero: " + genero + "\n";
		str += "Idioma: " + idioma + "\n";
		str += "Diretor: " + diretor + "\n";
		str += "Atores Principais: " + atoresPrincipais+ "\n";
		str += "Dura��o: " + duracao + "\n";
		str += "Ano: " + ano + "\n";
		
		return str;
	}

}
