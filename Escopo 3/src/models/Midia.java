package models;

import java.io.Serializable;

/**
 * Classe abstrata que representa uma midia.
 * 
 * @author grupo 3
 *
 */
public abstract class Midia implements Serializable{

	protected String nomeDoArquivo;

	protected String titulo;

	protected String descricao;

	protected String genero;

	protected int ano;

	/**
	 * M�todo construtor que recebe o nome do arquivo, o titulo do mesmo e
	 * define os outros atributos por padrao.
	 * 
	 * @param nomeDoArquivo
	 *            o nome do arquivo
	 * @param titulo
	 *            o titulo do arquivo
	 *
	 */
	public Midia(String nomeDoArquivo, String titulo) {
		this.nomeDoArquivo = nomeDoArquivo;
		this.titulo = titulo;
		this.descricao = "";
		this.genero = "";
		this.ano = 0000;
	}

	/**
	 * Metodo que seta o titulo do arquivo.
	 * 
	 * @param titulo
	 *            o novo titulo do arquivo
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * Metodo que recupera o titulo da midia.
	 * 
	 * @return o titulo da midia.
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * Metodo que retorna o nome do arquivo.
	 * 
	 * @return o nome do arquivo.
	 */
	public String getNomeDoArquivo() {
		return nomeDoArquivo;
	}

	/**
	 * Metodo redefine a descri��o da midia.
	 * 
	 * @param descricao
	 *            a nova descri��o da midia.
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Metodo que recupera a descri��o da midia.
	 * 
	 * @return a descri��o da midia.
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Metodo que redefine o genero da midia.
	 * 
	 * @param genero
	 *            o novo genero da midia.
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	 * Metodo que recupera o genero da midia.
	 * 
	 * @return o genero atual da midia.
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * Metodo que define o ano da midia.
	 * 
	 * @param ano
	 *            o ano.
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}

	/**
	 * Metodo que recupera o ano da midia.
	 * 
	 * @return o ano.
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * Metodo que retorna todas as informa�oes da midia contatenadas.
	 * 
	 * @return Todas as informa�oes da midia.
	 */
	public abstract String toString();

}
